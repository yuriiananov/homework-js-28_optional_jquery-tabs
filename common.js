"use strict";

$(document).ready(function(){
  $('.tabs-title:first').addClass('active');
  $('.tabs-content li:first').css('display', 'block');

  $('.tabs-title').on('click', function () {
    const selectedTab = $(this).data('name');

    $('.tabs-content li').each(function () {
        const contentName = $(this).data('name');
        $(this).css('display', contentName === selectedTab ? 'block' : 'none');
    });

    $('.tabs-title').removeClass('active');
    $(this).addClass('active');
  });
});

